<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>文档列表</title>
    <script type='text/javascript' src='/dcms/Core/Org/Jquery/jquery-1.8.2.min.js'></script>
	<link href='/dcms/Core/Org/hdjs/hdjs.css' rel='stylesheet' media='screen'>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/hdjs.min.js'></script>
	<script type='text/javascript' src='/dcms/Core/Org/hdjs/org/cal/lhgcalendar.min.js'></script>
	<script type='text/javascript'>
		MODULE='/dcms/index.php/Admin'; //当前模块
		CONTROLLER='/dcms/index.php/Admin/Article'; //当前控制器)
		ACTION='/dcms/index.php/Admin/Article/welcome';//当前方法(方法)
		ROOT='/dcms'; //当前项目根路径
		PUBLIC= '/dcms/Core/Tpcms/Admin/View/Public';//当前定义的Public目录
	</script>
    <link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.base.css" /><link rel="stylesheet" type="text/css" href="/dcms/Core/Org/ztree/css/zTreeStyle/zTreeStyle.css" /><link rel="stylesheet" type="text/css" href="/dcms/Core/Tpcms/Admin/View/Public/css/mod.article.css" />
    <script type="text/javascript">
        var ajaxCategoryUrl = "<?php echo U('Article/ajax_category');?>";
    </script>
    <script type="text/javascript" src="/dcms/Core/Org/ztree/js/jquery.ztree.all-3.5.min.js"></script><script type="text/javascript" src="/dcms/Core/Tpcms/Admin/View/Public/js/mod.article.js"></script>
</head>
<body>
    <div id="category_tree" style="display: block;">
        <div id="tree_title">
            <span></span>
            <a href="javascript:;" onclick="get_category_tree();" target="content">刷新栏目</a>
        </div>
        <ul id="treeDemo" class="ztree" style="top:25px;position: absolute;"></ul>
    </div>
    <div id="move">
        <span class="left"></span>
    </div>
    <div id="content">
        <iframe src="<?php echo U('Index/copyright');?>" name="content" scrolling="auto" frameborder="0" style="height:100%;width: 100%;"></iframe>
    </div>
<script type='text/javascript'>
    get_category_tree();
</script>
<style type="text/css">
    #tree_title span {
        display: block;
        background: url("/dcms/Core/Org/ztree/css/zTreeStyle/img/diy/1_open.png");
        width: 15px;
        height: 15px;
        float: left;
        margin-right: 5px;
    }
</style>

</body>
</html>