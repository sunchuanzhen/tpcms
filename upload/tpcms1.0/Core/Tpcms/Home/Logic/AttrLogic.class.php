<?php
/**[属性模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-28 19:24:42
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-02 00:08:51
 */
namespace Home\Logic;
use Think\Model;
class AttrLogic extends Model{

	private $cache;
	public function _initialize()
	{
		$this->cache = S('attr');
	}


	public function get_all($cid)
	{
		$ArticleAttrViewModel = D('ArticleAttrView');

		$cids = D('Category')->get_child_cid($cid);
		$data = $ArticleAttrViewModel->where(array('category_cid'=>array('in',$cids)))->field('attr_name,attr_id')->group('attr_attr_id')->order('attr.sort asc,attr.attr_id asc')->select();
	
		foreach ($data as $k => $v) 
		{
			$data[$k]['attr_value'] = $ArticleAttrViewModel->field('attr_value,attr_value_attr_value_id')->where(array('attr_id'=>$v['attr_id']))->group('attr_value_attr_value_id')->select();
		}
		return $data;
	}
}