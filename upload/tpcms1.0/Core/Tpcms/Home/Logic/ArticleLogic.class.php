<?php
/** [文档表模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-03-18 09:46:05
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-02 01:00:50
 */
namespace Home\Logic;
use Think\Model;
use Think\Page;
class ArticleLogic  extends Model{


	
	public function get_products($cid,$search,$sort)
	{
		

		$categoryModel = D('Category');
		$childCid = $categoryModel->get_child_cid($cid);
	

		

		/***扩展表名称***/
		$cur = D('Category','Service')->get_one($cid);
		$mid = $cur['model_mid'];
		// 读取表名称
		$model = D('Model','Service')->get_one($mid);
		$table = 'article_'.$model['name'];

		/***扩展字段***/
		// 显示在列表的扩展字段
		// 附表中要显示在列表中字段
		$modelFields = D('ModelField','Service')->get_all($model['mid']);
		$extfield = array();
		foreach($modelFields as $v)
		{
			if($v['show_lists'])
				$extfield[]=$v['fname'];
		}
		$extfield = implode(',', $extfield);
		// 视图模型添加附表关系
	 	

		$where = ' WHERE article.category_cid in ('. implode(',', $childCid) .') and article.verifystate = 2 ';


		$from = ' FROM '.C('DB_PREFIX').'article as article';
		$order = ' order  by '. $sort;
		$join = ' INNER JOIN '. C('DB_PREFIX').$table.' as '.$table;
		$on  = ' on '. $table.'.article_aid = article.aid ';


		/*两种情况
		1有属性筛选
		2没有属性筛选*/
		if($search)
		{
		
			foreach($search as $k=>$v)
			{
				$join .=' INNER JOIN '. C('DB_PREFIX').'article_attr as article_attr'.$k;
				$on .= ' AND article_attr'.$k.'.article_aid = article.aid';
				$where .= ' AND  article_attr'.$k.'.attr_value_attr_value_id = '.$v;
			}
		
		}
	

		$db = M();
		$countSql = "SELECT count(*) as c ".$from.$join.$on.$where;
		$count = $db->query($countSql);
		// 分页
		$page  = new Page($count[0]['c'],$cur['page']);
		// limit
		$limit = ' limit '. $page->firstRow.','.$page->listRows;
		if($extfield)
			$field = ' article.*,'.$extfield;
		else
			$field = 'article.*';
		$sql = "SELECT $field ". $from . $join . $on . $where . $order . $limit;
		$data = $db ->query($sql);


		if(!$data)
			return $data;
		foreach($data as $k=>$v)
		{
			$data[$k]['pic'] = $v['pic']?__ROOT__.'/'.$v['pic']:__ROOT__.'/Data/Public/images/default.gif';
		}


		 // 页脚信息
	    $page->setConfig('header',' record ');
		$page->setConfig('prev',' prev ');
		$page->setConfig('next',' next ');
		$page->setConfig('first',' first ');
		$page->setConfig('last',' last ');
		$page->setConfig('theme','%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');

	
		// 页脚信息
		$data['page'] = $page->show();

		
		return $data;
	}


	/**
	 * [get_sell 销售排行榜]
	 * @return [type] [description]
	 */
	public function get_sell()
	{
		$categoryService = D('Category',"Service");
		$childCid = $categoryService->get_child_cid(2);
		$where = ' WHERE article.category_cid in ('. implode(',', $childCid) .') and article.verifystate = 2 and goods_bought !=0';
		$order =' ORDER BY goods_bought DESC ';
		$limit = ' LIMIT 10 ';
		$sql ='SELECT article.*,goods.* FROM '.C('DB_PREFIX'). 'article as article JOIN '.C('DB_PREFIX').'goods as goods ON article.aid = goods.article_aid '.$where.$order.$limit;
		$db = M();
		$data =$db->query($sql);
	
		if(!$data)
			return $data;
		foreach($data as $k=>$v)
		{
			$data[$k]['pic'] = $v['pic']?__ROOT__.'/'.$v['pic']:__ROOT__.'/Data/Public/images/default.gif';
		}
		// p($data);
		return $data;


	}
}