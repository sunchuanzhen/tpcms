<?php
/** [附件管理]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-15 20:20:32
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-14 21:54:35
 */
namespace Admin\Controller;
class UploadController extends PublicController{

	public function del_no_use_attachment()
	{
		$db = D('Upload');
		$data = $db->where(array('article_aid'=>0))->select();
		foreach($data as $v)
		{
			$fullPath = $v['path'].'/'.$v['name'].'.'.$v['ext'];
			is_file($fullPath) && unlink($fullPath);
			$db->delete($v['id']);
		}
		
		$this->success('删除成功',U('index'));
	}

}