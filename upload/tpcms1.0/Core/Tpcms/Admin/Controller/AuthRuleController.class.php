<?php
/**[规则控制器]
 * @Author: chenli
 * @Email:  976123967@qq.com
 * @Date:   2015-02-23 11:53:41
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 19:16:19
 */
namespace Admin\Controller;
class AuthRuleController extends PublicController{
	public function _initialize()
	{
		parent::_initialize();
		$rules = $this->logic->get_all();
		$this->assign('rules',$rules);
	}
	public function index()
	{
		$this->display();
	}
}