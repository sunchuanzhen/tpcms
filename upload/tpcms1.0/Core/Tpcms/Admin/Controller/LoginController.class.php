<?php
/**[登录控制器]
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-03-14 15:01:04
 * @Last Modified by:   happy
 * @Last Modified time: 2015-05-01 16:43:22
 */
namespace Admin\Controller;
use Common\Controller\ExtendController;
class LoginController extends ExtendController{

	/**
	 * [_initialize 初始化]
	 * @return [type] [description]
	 */
	public function _initialize()
	{
		$this->logic = D('User','Logic');
	}

	/**
	 * [ajax_check_code 验证码比较]
	 * @return [type] [description]
	 */
	public function ajax_check_code()
	{
		if(!$this->logic->check_code())
			$this->ajaxReturn(array('getdata'=>'false'));
		else
			$this->ajaxReturn(array('getdata'=>'true'));
	}

	/**
	 * [ajax_check_login 验证登录]
	 * @return [type] [description]
	 */
	public function ajax_check_login()
	{
		if(!$user = $this->logic->validate_login())
			$this->ajaxReturn(array('status'=>0,'info'=>$this->logic->getError()));
		else
		{
			// 更新登录信息
			$this->logic->update_login($user);
			// 设置session
			$this->logic->set_session($user);
			$this->ajaxReturn(array('status'=>1));
		}
	}


	/**
	 * [out 退出]
	 * @return [type] [description]
	 */
	public function out()
	{
		session('user_name',null);
		session('user_id',null);
		$this->redirect('Login/index');
	}
}