<?php
/** [会员等级模型]
 * @Author: 976123967@qq.com
 * @Date:   2015-04-16 09:22:27
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-04 10:33:21
 */
namespace Admin\Logic;
use Think\Model;
class UserGradeLogic extends Model{

	private $cache;

	public function _initialize()
	{
		$this->cache = S('grade');
	}
	/**
	 * [get_all 所有分组]
	 * @return [type] [description]
	 */
	public function get_all()
	{
		return $this->cache;
	}

	/**
	 * [update_cache 更新缓存]
	 * @return [type] [description]
	*/
	public function update_cache()
	{
		$data = $this->order(array('gid'=>'asc'))->select();
		$temp = array();
		foreach($data as $k=>$v)
		{
			$temp[$v['gid']]=$v;
		}
		S('grade',$temp);
	}

	/**
	 * [get_one 读取一条记录]
	 * @param  [type] $gid [description]
	 * @return [type]      [description]
	 */
	public function get_one($gid)
	{
		return isset($this->cache[$gid])?$this->cache[$gid]:null;
	}

	/**
	 * [_after_insert 添加后置方法]
	 * @param  [type] $data    [description]
	 * @param  [type] $options [description]
	 * @return [type]          [description]
	 */
	public function _after_insert($data,$options)
	{
		// 更新缓存
		$this->update_cache();
	}
	
	/**
	 * [_after_update 更新后置方法]
	 * @param  [type] $data    [description]
	 * @param  [type] $options [description]
	 * @return [type]          [description]
	 */
	public function _after_update($data,$options)
	{
		// 更新缓存
		$this->update_cache();
	}

	/**
	 * [del 删除]
	 * @param  [type] $gid [description]
	 * @return [type]      [description]
	 */
	public function del($gid)
	{
		$status = D('User')->where(array('grade_gid'=>$gid))->getField('uid');
		if($status)
		{
			$this->error='会员等级使用中';
			return false;
		}
		$this->delete($gid);
		$this->update_cache();
		return true;
	}


}