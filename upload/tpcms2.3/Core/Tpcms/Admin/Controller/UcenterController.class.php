<?php
/**网站配置控制器
 * @Author: happy
 * @Email:  976123967@qq.com
 * @Date:   2015-07-14 22:01:43
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-07-24 13:30:32
 */
namespace Admin\Controller;
class UcenterController extends PublicController{



    /**
     * [index 配置列表]
     * @return [type] [description]
     */
    public function index()
    {
        if(C('cfg_ucenter'))
        {
            $ucenter = include_once './Data/Config/ucenter.inc.php';
            $this->assign('ucenter',$ucenter);
            $this->display();
        }
        else
        {
            // 提示
            $this->assign('message','请先到系统>站点设置>更多设置中开启ucenter');
            $this->display('./Data/Public/notice/auth.html');
            die;
        }

    }

    // 设置信息
    public function setucenter()
    {
        if(IS_POST)
        {
            $dbhost =I('post.dbhost');
            $dbuser =I('post.dbuser');
            $dbpw =I('post.dbpw');
            $dbname =I('post.dbname');
            $tablepre =I('post.tablepre');
            $uckey=I('post.uc_key');
            $ucapi=I('post.uc_api');

            //验证数据信息是否正确
            if(!$lnk=mysql_connect($dbhost,$dbuser,$dbpw))
            {
                $this->error('数据库连接信息错误！');
            }
            //验证数据库是否存在
            if(!mysql_select_db($dbname, $lnk))
            {
                $this->error('数据库不存在！');
            }

           //保存配置数据
            $ucenter= array(
                'dbhost'=>$dbhost,
                'dbuser'=>$dbuser,
                'dbpw'=>$dbpw,
                'dbname'=>$dbname,
                'tablepre'=>$tablepre,
                'uc_key'=>$uckey,
                'uc_api'=>$ucapi
            );
           $status =  file_put_contents('./Data/Config/ucenter.inc.php',"<?php \r\n return ".var_export($ucenter,true).";\r\n ?>");
           if(!$status) $this->error('请检查'.realpath('./Data/Config/ucenter.inc.php').'文件权限');





            //验证本地的数据库中的user表的数据是否导入到members
            $userModel = D('User');
            $user=$userModel->select();

            S('user',$user);
            $this->success('设置完成，开始同步',U('synchronous'));

        }


    }


    //同步
    public function synchronous()
    {
        $action = S('user');
        $current =array_shift($action);
        $this->assign('waitSecond',1);

        S('user',$action);


        $ucenter = include_once './Data/Config/ucenter.inc.php';
        $memberModel = new \Think\Model('Members',$ucenter['tablepre'],'mysql://'.$ucenter['dbuser'].':'.$ucenter['dbpw'].'@'.$ucenter['dbhost'].'/'.$ucenter['dbname'].'#utf8');


        //链接数据库
        $data = $memberModel->where(array('username'=>$current['username']))->find();

        //数据不存在
        if(!$data)
        {
            $user = array(
                'username'=>$current['username'],
                'password'=>MD5($current['password']),
                'email'=>$current['email'],
                'regdate'=>time(),
                'salt'=>'',
            );
            //要保证ucenter members表中的uid 和 tpcms 中user表中uid同步
            $uid = $memberModel->add($user);

        }
        else
        {
            //更新密码、邮箱保证能够的登录
            $user = array(
                'password'=>MD5($current['password']),
                'email'=>$current['email'],
                'salt'=>'',
            );
            $memberModel->where(array('uid'=>$data['uid']))->save($user);
            //要保证ucenter members表中的uid 和 tpcms 中user表中uid同步
            $uid = $data['uid'];
        }

        //要保证ucenter members表中的uid 和 tpcms 中user表中uid同步
        $sql = 'update '.C('DB_PREFIX').'user set uid='.$uid.' where username="'.$current['username'].'"';
        M()->execute($sql);

        $sql = 'update '.C('DB_PREFIX').'user_baseinfo set user_uid='.$uid.' where user_uid='.$current['uid'];
        M()->execute($sql);

        if(empty($action))
        {
            $this->success('同步完成',U('index'));
            die;
        }
        else
        {
            $this->success('正在同步会员：'.$current['username'],U('synchronous'));
        }
    }


}