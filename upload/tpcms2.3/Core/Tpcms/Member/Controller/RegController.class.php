<?php
/**[注册]
 * @Author: 976123967@qq.com
 * @Date:   2015-05-11 12:02:40
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-05-11 15:44:14
 */
namespace Member\Controller;
use Common\Controller\CommonController;
class RegController extends CommonController{



	public function index()
	{
		$db = D('User','Logic');
		if(IS_POST)
		{
			if(!$db->create())
			{
				$this->error($db->getError());
			}


			//ucenter注册
			if(C('cfg_ucenter'))
			{

				include_once './Data/Config/uc.inc.php';
				include_once './uc_client/client.php';

			

				// 不存在ucenter中 不存在应用中
				$uid = uc_user_register(I('post.username'), I('post.password'), I('post.email'));

				/*大于 0:返回用户 ID，表示用户注册成功
				-1:用户名不合法
				-2:包含不允许注册的词语
				-3:用户名已经存在
				-4:Email 格式有误
				-5:Email 不允许注册
				-6:该 Email 已经被注册*/
				switch ($uid)
				{
					case -1:
						$this->error('用户名不合法');
						break;
					case -2:
						$this->error('包含不允许注册的词语');
						break;
					case -3:
						$this->error('用户名已经存在');
						break;
					case -4:
						$this->error('Email 格式有误');
						break;
					case -5:
						$this->error('Email 不允许注册');
						break;
					case -6:
						$this->error('该 Email 已经被注册');
						break;
					default:
						$username = I('post.username');
						break;
				}

				$db->uid = $uid;


				setcookie('tpcms_auth', uc_authcode($uid."\t".$username, 'ENCODE'),0,'/');
				$ucsynlogin = uc_user_synlogin($uid);
				echo $ucsynlogin;
			}


			$uid = $db->add();

			session('uid',$uid);
			session('username',I('post.username'));
			session('email',I('post.email'));



			$this->success('注册成功',U('Member/User/index'));
		}
		else
		{
			//判断是否已经登录
			$this->_check_ucenter();


			$cms = $this->base();
			$this->assign('cms',$cms);
			$this->display();
		}
	}


	public function ajax_username()
	{
		$username = I('get.username');
		$status = D('User','Logic')->check_username($username);
		if(!$status)
			echo "false";
		else
			echo "true";
		die;
	}
	public function ajax_email()
	{
		$email = I('get.email');

		$status = D('User','Logic')->check_email($email);
		if(!$status)
			echo "false";
		else
			echo "true";
		die;
	}

}